package com.example.thinkpad.login;
import android.os.AsyncTask;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class DBHandler extends AsyncTask<Object,Void,String>  {
    //Delegate helps us get response in postExecute with DBHandler still being async.
    public IAsyncResponse delegate = null;

    @Override
    protected String doInBackground(Object... params) {
        String response = "No Response";
        String reqType = (String)params[0];
        String url = (String)params[1];
        HashMap<String, String> dbInput = (HashMap<String, String>)params[2];
        String dbInputStr = dataHandler(reqType, url, dbInput);
        if(reqType.equals("GET")){
            response = GETrequest(url, dbInputStr);
        }else if(reqType.equals("POST")){
            response = POSTrequest(url, dbInputStr);
        }
        return response;
    }
    //now we can get the response through our asyncRespone interface.
    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }
    //handle data from HashMap so it's ready for a GET og POST request.
    public String dataHandler(String reqType, String url, HashMap<String, String> data) {
        String dataStr = "";
        for (Map.Entry<String, String> entry : data.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (reqType.equals("GET")) {
                dataStr += key + "=" + value + "&";
            } else if (reqType.equals("POST")) {
                try {
                    dataStr += URLEncoder.encode(key, "UTF-8") +
                            "=" + URLEncoder.encode(value, "UTF-8") + "&";
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        Log.d("dataHandler(to be send)", url + " " + dataStr);
        return dataStr;
    }
    private String GETrequest(String urlStr, String dbInputStr) {
        String response = "No GET response";
        try{
            URL url = new URL(urlStr+dbInputStr);
            URLConnection conn = (HttpURLConnection) url.openConnection();
            response = getResponse(conn);
        }catch(Exception e){}
        return response;
    }
    public String POSTrequest(String urlStr, String dbInputStr){
        String response = "No POST response";
        try {
            String data = "";
            data = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode("banan", "UTF-8");
            data += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode("pw123", "UTF-8");
            URL url = new URL(urlStr);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(dbInputStr);
            wr.flush();
            response = getResponse(conn);
        }catch (Exception e){}
        return response;
    }
    //read response and put it together as a String
    public String getResponse(URLConnection conn) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        StringBuffer sb = new StringBuffer();
        while ((line = in.readLine()) != null) {
            sb.append(line);
        }
        in.close();
        String response  = sb.toString();
        return response;
    }
}


