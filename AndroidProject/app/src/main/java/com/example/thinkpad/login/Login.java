package com.example.thinkpad.login;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;

public class Login extends AppCompatActivity implements IAsyncResponse {
    public final static String tokenID = "com.example.thinkpad.login";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText usernameInput = (EditText) findViewById(R.id.usernameBox);
                EditText passwordInput = (EditText) findViewById(R.id.passwordBox);
                String username = usernameInput.getText().toString();
                String password = passwordInput.getText().toString();
                login(username, password);
            }
        });
    }
    public void login(String username, String password) {
        String kusername = "banan";
        String kpassword = "pw123";
        String url = "http://10mail.dk/eksSys/android/loginAndroidPost.php";
        String reqType = "POST";
        HashMap<String, String> dbInput = new HashMap();
        dbInput.put("username", kusername);
        dbInput.put("password", kpassword);
        //The async task can be executed only once, so a new instance is created
        DBHandler dbHandler = new DBHandler();
        dbHandler.delegate = this;
        dbHandler.execute(reqType, url, dbInput);
    }

    @Override
    public void processFinish(String output) {
        Log.d("PF", output);
        try {
            JSONObject jsonObject = new JSONObject(output);
            String tokenStr = jsonObject.getString("Token");
            String loginStr = jsonObject.getString("login status");
            attemplogin(loginStr, tokenStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void attemplogin(String loginStr, String tokenStr){
        if (loginStr.equals("Succes")) {
            Log.d("tStr", tokenStr);
            Token token = new Token(tokenStr, "expStr");
            Log.d("login", "lol"+token.getToken());

            Intent intent = new Intent(Login.this, PersonalActivity.class);
            //Stat and activity with parameters
            intent.putExtra(tokenID, token);
            startActivity(intent);
        } else {
            TextView loginTextView = (TextView) findViewById(R.id.loginTextView);
            loginTextView.setVisibility(View.VISIBLE);
        }
    }
}
