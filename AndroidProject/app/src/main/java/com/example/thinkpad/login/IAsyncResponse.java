package com.example.thinkpad.login;

public interface IAsyncResponse {
    void processFinish(String output);
}
