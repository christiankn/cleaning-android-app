package com.example.thinkpad.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.Serializable;

public class PersonalActivity extends AppCompatActivity {
    private Token token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal);
        Intent i = getIntent();
        token = (Token)i.getExtras().getSerializable(Login.tokenID);
        token.getToken();
    }
}
