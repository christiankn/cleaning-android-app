package com.example.thinkpad.login;
import java.io.Serializable;
public class Token implements Serializable {
    private String token;
    private String exp;
    public Token(String token, String exp){
        this.token = token;
        this.exp = exp;
    }
    public String getToken(){
        return token;
    }
    public String getExp(){
        return exp;
    }
}
